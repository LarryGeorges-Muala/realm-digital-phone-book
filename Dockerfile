FROM ubuntu:18.04

WORKDIR /phone_book

COPY requirements.txt ./

RUN apt-get update -y

RUN apt-get upgrade -y

RUN apt-get install -y python3.6

RUN apt-get install -y python3-pip

RUN apt-get install -y python3-dev libpython3-dev

RUN DEBIAN_FRONTEND=noninteractive apt-get install -y tzdata

RUN echo "Africa/Johannesburg" > /etc/timezone

RUN dpkg-reconfigure -f noninteractive tzdata

RUN apt-get install build-essential -y

RUN apt-get install -y curl

RUN apt-get install -y python3.6-minimal \
        && apt-get install python-pip -y

RUN pip3 install -r requirements.txt

COPY . .

# Clean Unnecessary Files
RUN apt-get clean autoclean
RUN apt-get autoremove -y
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN rm -rf /var/lib/{apt,dpkg,cache,log}/

RUN python3 manage.py check

RUN python3 manage.py migrate

RUN python3 manage.py collectstatic --no-input

ENV PYTHONPATH "${PYTHONPATH}:/phone_book"

# Expose Ports
EXPOSE 5432 3306 8000 8080 80 443

# Run App
CMD ["python3", "manage.py", "runserver", "0.0.0.0:8000"]