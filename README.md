## Project Details

**Languages Stack**

- Built with **Python3.6** and **Django1.11**
- Deployed using **Docker** and **Bitbucket Pipeline**
- Can be run locally using **Django** internal server with **python manage.py runserver** on url **127.0.0.1:8000**
- Can be run locally using **Docker Compose** and the command **docker-compose up**
- Front-End handled with **HTML**, **CSS**, **Bootstrap** and **plain Javascript**
- Project running **MySQL DB** (Local dump - **db.sqlite3** - already available for quick setup and data browsing)

**To execute**

1. Clone the repo
2. Create a Python Virtual Environment for the project's components with **python -m venv .env**
3. Activate Virtual Environment with **source .env/bin/activate**
4. Install project's components with **pip install -r requirements.txt**
5. Initialize local database with **python manage.py migrate**
6. Test project's setup with **python manage.py check**
7. Start local server to access project using **python manage.py runserver**
8. Access project on any internet browser via the URL **127.0.0.1:8000**
