'''
	Unit Tests
'''
import unittest
from django.test import TestCase
from phone_book import views


class DatabaseModelTest(unittest.TestCase):
	'''
		Local Test command - python manage.py test
	'''
	def test_models(self):
		db_test = views.test_database_tables()
		self.assertEqual(db_test, True)
