import datetime
from django.db import models

class PhoneBookUsers(models.Model):
    ''' Phone Book Users '''
    user_name = models.CharField(max_length=200, null=True, blank=True, default='')

    def secure_time_under_timezone():
        return datetime.datetime.now(datetime.timezone.utc)
    creation_date = models.DateTimeField('Creation Date', default=secure_time_under_timezone)

    def __str__(self):
        return self.user_name
    

class PhoneBookContacts(models.Model):
    ''' Contacts Linked To Phone Book Users '''
    user_reference = models.ForeignKey(PhoneBookUsers, on_delete=models.CASCADE)
    contact_first_name = models.CharField(max_length=200, null=True, blank=True, default='')
    contact_last_name = models.CharField(max_length=200, null=True, blank=True, default='')

    def generate_contact_full_name(self):
        # Capitalize First Name
        capitalized_first_name = ''
        for entry in str(self.contact_first_name).split(' '):
            if not capitalized_first_name:
                capitalized_first_name += str(entry).capitalize()
            else:
                capitalized_first_name += ' {}'.format(str(entry).capitalize())
        # Capitalize Last Name
        capitalized_last_name = ''
        for entry in str(self.contact_last_name).split(' '):
            if not capitalized_last_name:
                capitalized_last_name += str(entry).capitalize()
            else:
                capitalized_last_name += ' {}'.format(str(entry).capitalize())
        # Build Full Name
        return '{} {}'.format(
            capitalized_first_name,
            capitalized_last_name
        )

    def generate_contact_numbers(self):
        contact_numbers = ''
        contact_numbers_check = self.phonebookcontactnumber_set.all()
        if contact_numbers_check:
            for number in contact_numbers_check:
                contact_numbers += '|{}'.format(number)
        return contact_numbers

    def generate_contact_emails(self):
        contact_emails = ''
        contact_emails_check = self.phonebookcontactemail_set.all()
        if contact_emails_check:
            for email in contact_emails_check:
                contact_emails += '|{}'.format(email)
        return contact_emails

    def secure_time_under_timezone():
        return datetime.datetime.now(datetime.timezone.utc)
    creation_date = models.DateTimeField('Creation Date', default=secure_time_under_timezone)

    def __str__(self):
        return '{} {}'.format(self.contact_first_name, self.contact_last_name)


class PhoneBookContactNumber(models.Model):
    ''' Numbers Linked To Contacts '''
    contact_reference = models.ForeignKey(PhoneBookContacts, on_delete=models.CASCADE)
    contact_number = models.CharField(max_length=200, null=True, blank=True, default='')

    def secure_time_under_timezone():
        return datetime.datetime.now(datetime.timezone.utc)
    creation_date = models.DateTimeField('Creation Date', default=secure_time_under_timezone)

    def __str__(self):
        return self.contact_number


class PhoneBookContactEmail(models.Model):
    ''' Emails Linked To Contacts '''
    contact_reference = models.ForeignKey(PhoneBookContacts, on_delete=models.CASCADE)
    contact_email = models.CharField(max_length=200, null=True, blank=True, default='')

    def secure_time_under_timezone():
        return datetime.datetime.now(datetime.timezone.utc)
    creation_date = models.DateTimeField('Creation Date', default=secure_time_under_timezone)

    def __str__(self):
        return self.contact_email
