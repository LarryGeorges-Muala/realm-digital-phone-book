from django.shortcuts import render
from django.conf import settings
from django.http import HttpResponseRedirect, HttpRequest, JsonResponse
from django.views.decorators.csrf import csrf_exempt
try:
	from django.urls import reverse
except:
	from django.core.urlresolvers import reverse
from django.utils import timezone
from phone_book import models


def create_sample_phone_books():
	models.PhoneBookUsers.objects.create(
		user_name='Public'
	)
	models.PhoneBookUsers.objects.create(
		user_name='Private'
	)
	return True


def save_verified_id(request, user_id):
	# Save Verified ID
	try:
		if request.session.has_key('user_id'):
			del request.session['user_id']
		request.session['user_id'] = int(user_id)
		return True
	except Exception as error:
		print(error)
	return False


@csrf_exempt
def index(request):
	# create_sample_phone_books()

	if request.method == "POST":
		# Create New Phone Book
		passed_name = request.POST.get('user-name', '')
		new_user = models.PhoneBookUsers.objects.create(
			user_name=passed_name
		)
		save_verified_id(request, new_user.id)
		return HttpResponseRedirect(reverse('phone_book:index'))

	# Check Existing Users
	users = models.PhoneBookUsers.objects.all()
	users_list = []

	for single_user in users.order_by('user_name'):
		temp_obj = {
			'id': single_user.id,
			'name': single_user.user_name
		}
		users_list.append(temp_obj)

	context = {
		'users': users_list
	}
	return render(request, 'phone_book/index.html', context)


@csrf_exempt
def contacts(request):

	if request.method == "POST":
		# User Details
		user_id = request.POST.get('user-id', '')
		
		# Verify User
		user_id = int(user_id)
		user_check = models.PhoneBookUsers.objects.filter(id=user_id)

		if user_check:
			user_check = user_check.order_by('id').first()
		else:
			return HttpResponseRedirect(reverse('phone_book:index'))

		# Save Verified ID
		try:
			if request.session.has_key('user_id'):
				del request.session['user_id']
			request.session['user_id'] = int(user_id)
		except Exception as error:
			print(error)
		return HttpResponseRedirect(reverse('phone_book:contacts'))

	contacts_list = []
	phone_book_user = ''

	# Check Logged In User
	if request.session.has_key('user_id'):
		saved_user_id = request.session['user_id']

		# Get User Contact List
		saved_user_id = int(saved_user_id)
		user_check = models.PhoneBookUsers.objects.filter(id=saved_user_id)

		if user_check:
			user_check = user_check.order_by('id').first()

			phone_book_user = {
				'id': user_check.id,
				'name': user_check.user_name
			}

			contact_list_check = models.PhoneBookContacts.objects.filter(
				user_reference=user_check
			)
			if contact_list_check:
				for contact in contact_list_check.order_by('contact_first_name'):
					# Get Contact Numbers
					extracted_contact_numbers = []
					contact_numbers_list_check = models.PhoneBookContactNumber.objects.filter(
						contact_reference=contact
					)
					if contact_numbers_list_check:
						for number in contact_numbers_list_check:
							if number.contact_number:
								temp_number_obj = {
									'id': number.id,
									'number': number.contact_number
								}
								extracted_contact_numbers.append(temp_number_obj)

					# Get Contact Emails
					extracted_contact_emails = []
					contact_emails_list_check = models.PhoneBookContactEmail.objects.filter(
						contact_reference=contact
					)
					if contact_emails_list_check:
						for email in contact_emails_list_check:
							if email.contact_email:
								temp_email_obj = {
									'id': email.id,
									'email': email.contact_email
								}
								extracted_contact_emails.append(temp_email_obj)

					temp_obj = {
						'id': contact.id,
						'name': contact.generate_contact_full_name(),
						'first_name': contact.contact_first_name,
						'last_name': contact.contact_last_name,
						'contact_numbers': extracted_contact_numbers,
						'contact_numbers_list': contact.generate_contact_numbers(),
						'contact_emails': extracted_contact_emails,
						'contact_emails_list': contact.generate_contact_emails()
					}
					contacts_list.append(temp_obj)
	else:
		return HttpResponseRedirect(reverse('phone_book:index'))

	context = {
		'user': phone_book_user,
		'contacts_list': contacts_list
	}
	return render(request, 'phone_book/contacts.html', context)


@csrf_exempt
def edit_contacts(request):
	if request.method == "POST":
		# Contact details
		contact_id = request.POST.get('contact-edit-id', '')
		contact_first_name = request.POST.get('contact-edit-first-name', '')
		contact_last_name = request.POST.get('contact-edit-last-name', '')
		contact_numbers = request.POST.get('contact-edit-numbers', '')
		contact_emails = request.POST.get('contact-edit-emails', '')

		try:
			# Get Contact
			target_contact = models.PhoneBookContacts.objects.filter(
				id=int(contact_id)
			)
			if target_contact:
				target_contact = target_contact.order_by('id').first()

				# Update Contact
				target_contact.contact_first_name = contact_first_name
				target_contact.contact_last_name = contact_last_name
				target_contact.save()

				# Update Numbers
				if contact_numbers:
					for entry in target_contact.phonebookcontactnumber_set.all():
						entry.delete()

					contact_numbers = str(contact_numbers).split('|')
					for number in contact_numbers:
						if number:
							models.PhoneBookContactNumber.objects.create(
								contact_reference=target_contact,
								contact_number=number
							)

				# Update Emails
				if contact_emails:
					for entry in target_contact.phonebookcontactemail_set.all():
						entry.delete()

					contact_emails = str(contact_emails).split('|')
					for email in contact_emails:
						if email:
							models.PhoneBookContactEmail.objects.create(
								contact_reference=target_contact,
								contact_email=email
							)

		except Exception as error:
			print(error)

	return HttpResponseRedirect(reverse('phone_book:contacts'))


@csrf_exempt
def create_contacts(request):
	if request.method == "POST":
		# Contact details
		phone_book_user_id = request.POST.get('new-contact-phone-book-id', '')
		new_contact_first_name = request.POST.get('new-contact-first-name', '')
		new_contact_last_name = request.POST.get('new-contact-last-name', '')
		new_contact_numbers = request.POST.get('new-contact-numbers', '')
		new_contact_emails = request.POST.get('new-contact-emails', '')

		try:
			# Get Phone Book
			phone_book_user = models.PhoneBookUsers.objects.filter(
				id=int(phone_book_user_id)
			)
			if phone_book_user:
				phone_book_user = phone_book_user.order_by('id').first()

				# Create Contact
				target_contact = models.PhoneBookContacts.objects.create(
					user_reference=phone_book_user,
					contact_first_name=new_contact_first_name,
					contact_last_name=new_contact_last_name
				)

				# Add Contact Numbers
				if new_contact_numbers:
					# Extract Numbers
					new_contact_numbers = str(new_contact_numbers).split('|')
					for number in new_contact_numbers:
						if number:
							models.PhoneBookContactNumber.objects.create(
								contact_reference=target_contact,
								contact_number=number
							)

				# Add Contact Emails
				if new_contact_emails:
					# Extract Emails
					new_contact_emails = str(new_contact_emails).split('|')
					for email in new_contact_emails:
						if email:
							models.PhoneBookContactEmail.objects.create(
								contact_reference=target_contact,
								contact_email=email
							)
		except Exception as error:
			print(error)

	return HttpResponseRedirect(reverse('phone_book:contacts'))


@csrf_exempt
def delete_contacts(request):
	if request.method == "POST":
		# Contact details
		contact_id = request.POST.get('contact-delete-id', '')

		try:
			# Get Contact
			target_contact = models.PhoneBookContacts.objects.filter(
				id=int(contact_id)
			)
			if target_contact:
				target_contact = target_contact.order_by('id').first()

				# Delete Contact
				target_contact.delete()
		except Exception as error:
			print(error)

	return HttpResponseRedirect(reverse('phone_book:contacts'))


@csrf_exempt
def delete_contact_number(request):
	if request.method == "POST":
		# Contact details
		contact_id = request.POST.get('contact-delete-number-id', '')
		contact_number = request.POST.get('contact-delete-number', '')

		try:
			# Get Contact
			target_contact = models.PhoneBookContacts.objects.filter(
				id=int(contact_id)
			)
			if target_contact:
				target_contact = target_contact.order_by('id').first()

				# Delete Number
				if contact_number:
					target_number = models.PhoneBookContactNumber.objects.filter(
						contact_reference=target_contact,
						contact_number=contact_number
					)
					if target_number:
						for entry in target_number:
							entry.delete()
		except Exception as error:
			print(error)

	return HttpResponseRedirect(reverse('phone_book:contacts'))


@csrf_exempt
def delete_contact_email(request):
	if request.method == "POST":
		# Contact details
		contact_id = request.POST.get('contact-delete-email-id', '')
		contact_email = request.POST.get('contact-delete-email', '')

		try:
			# Get Contact
			target_contact = models.PhoneBookContacts.objects.filter(
				id=int(contact_id)
			)
			if target_contact:
				target_contact = target_contact.order_by('id').first()

				# Delete Email
				if contact_email:
					target_email = models.PhoneBookContactEmail.objects.filter(
						contact_reference=target_contact,
						contact_email=contact_email
					)
					if target_email:
						for entry in target_email:
							entry.delete()
		except Exception as error:
			print(error)

	return HttpResponseRedirect(reverse('phone_book:contacts'))


@csrf_exempt
def edit_user_phone_book(request):
	if request.method == "POST":
		# Phone Book Details
		phone_book_id = request.POST.get('phone-book-id', '')
		edited_phone_book_name = request.POST.get('phone-book-name', '')

		try:
			# Get Phone Book
			target_phone_book = models.PhoneBookUsers.objects.filter(
				id=int(phone_book_id)
			)
			if target_phone_book:
				target_phone_book = target_phone_book.order_by('id').first()

				# Update Phone Book
				target_phone_book.user_name = edited_phone_book_name
				target_phone_book.save()
		except Exception as error:
			print(error)

	return HttpResponseRedirect(reverse('phone_book:contacts'))


@csrf_exempt
def delete_user_phone_book(request):
	if request.method == "POST":
		# Contact details
		phone_book_id = request.POST.get('phone-book-delete-id', '')

		try:
			# Get Phone Book
			target_phone_book = models.PhoneBookUsers.objects.filter(
				id=int(phone_book_id)
			)
			if target_phone_book:
				target_phone_book = target_phone_book.order_by('id').first()

				# Delete Phone Book
				target_phone_book.delete()
		except Exception as error:
			print(error)

	return HttpResponseRedirect(reverse('phone_book:index'))


def test_database_tables():
	try:
		phone_book_test = models.PhoneBookUsers.objects.create(
			user_name='Public'
		)

		if phone_book_test:
			test_contact = models.PhoneBookContacts.objects.create(
				user_reference=phone_book_test,
				contact_first_name='Test',
				contact_last_name='Test'
			)
			if test_contact:
				test_contact.delete()
			phone_book_test.delete()
			return True
	except Exception as error:
		print(error)
	return False
