from django.conf.urls import url 
from . import views

app_name = 'phone_book'

urlpatterns=[
	url(r'^$', views.index, name='index'),
	url(r'^contacts/$', views.contacts, name='contacts'),
	url(r'^contacts/edit/$', views.edit_contacts, name='edit_contacts'),
	url(r'^contacts/delete/$', views.delete_contacts, name='delete_contacts'),
	url(r'^contacts/delete/number/$', views.delete_contact_number, name='delete_contact_number'),
	url(r'^contacts/delete/email/$', views.delete_contact_email, name='delete_contact_email'),
	url(r'^contacts/create/$', views.create_contacts, name='create_contacts'),
	url(r'^phone_book/edit/$', views.edit_user_phone_book, name='edit_user_phone_book'),
	url(r'^phone_book/delete/$', views.delete_user_phone_book, name='delete_user_phone_book'),
]